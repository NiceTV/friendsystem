package me.nicedev.droopmc.friend.commands;

import java.util.UUID;

import me.nicedev.droopmc.friend.utils.ArgumentCommand;
import me.nicedev.droopmc.friend.utils.FriendPlayer;
import me.nicedev.droopmc.friend.utils.NameFetcher;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class AddCommand extends ArgumentCommand {

	public AddCommand() {
		super("add");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(args.length < 1) {
			sender.sendMessage(p.prefix + "�cVerwendung: /friend add <Name>");
			return;
		}
		
		FriendPlayer player = FriendPlayer.getPlayer(((ProxiedPlayer) sender).getUniqueId());
		UUID target = NameFetcher.getUUID(args[0]);
		
		if(target == null) {
			sender.sendMessage(p.prefix + "�cUng�ltiger Spielername!");
			return;
		}
		
		if(!FriendPlayer.hasPlayedBefore(target)) {
			sender.sendMessage(p.prefix + "�cDieser Spieler hat noch nie auf dem Server gespielt!");
			return;
		}
		
		if(player.getInvites().contains(target)) {
			sender.sendMessage(p.prefix + "�cDu hast dem Spieler bereits eine Freundschaftsanfrage geschickt!");
			return;
		}
		
		if(player.getFriends().contains(target)) {
			sender.sendMessage(p.prefix + "�cDieser Spieler ist bereits dein Freund!");
			return;
		}
		
		if(!FriendPlayer.getPlayer(target).isAcceptingInvites()) {
			sender.sendMessage(p.prefix + "�cDieser Spieler nimmt keine Freundschaftsanfragen an!");
			return;
		}
		
		
	}

}
