package me.nicedev.droopmc.friend.commands;

import java.util.List;
import java.util.UUID;

import me.nicedev.droopmc.friend.utils.ArgumentCommand;
import me.nicedev.droopmc.friend.utils.FriendPlayer;
import me.nicedev.droopmc.friend.utils.NameFetcher;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class ListCommand extends ArgumentCommand {

	public ListCommand() {
		super("list");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		int site = 0;
		if(args.length == 0) {
			site = 1;
		} else {
			try {
				site = Integer.parseInt(args[0]);
			} catch(NumberFormatException e) {
				sender.sendMessage(p.prefix + "�cDies ist keine g�ltige Seite!");
				return;
			}
		}
		
		List<UUID> friends = FriendPlayer.getPlayer(((ProxiedPlayer) sender).getUniqueId()).getFriends();
		int sites = friends.size() / 8 + (friends.size() % 8 > 0 ? 1 : 0);
		
		if(site > sites || site < 1) {
			sender.sendMessage(p.prefix + "�cDies ist keine g�ltige Seite!");
			return;
		}
		
		sender.sendMessage(p.prefix + "�6Deine Freunde �8(�a" + site + "/�8" + sites + ")");
		
		for(int i = (site - 1) * 8; i <= site * 8; i++) {
			int index = i - 1;
			
			ProxiedPlayer player = p.getProxy().getPlayer(friends.get(index));
			String status = player == null ? "�8Offline" : "�aOnline auf �8" + player.getServer().getInfo().getName();
			sender.sendMessage(" �5" + NameFetcher.getName(friends.get(index)) + " - " + status);
		}
	}

}
