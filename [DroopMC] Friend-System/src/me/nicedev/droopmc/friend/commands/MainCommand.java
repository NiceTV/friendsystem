package me.nicedev.droopmc.friend.commands;

import java.util.Arrays;

import me.nicedev.droopmc.friend.utils.BaseCommand;
import net.md_5.bungee.api.CommandSender;

public class MainCommand extends BaseCommand {

	public MainCommand() {
		super("friend", Arrays.asList());
	}

	@Override
	public void executeCommand(CommandSender sender, String[] args) {
		sendHelpMessage(sender, "/friend list <Seite>", "Zeigt eine Liste aller Freunde");
		sendHelpMessage(sender, "/friend add <Name>", "F�gt einen neuen Freund hinzu");
		sendHelpMessage(sender, "/friend remove <Name>", "Entfernt einen Freund");
		sendHelpMessage(sender, "/friend clear", "Leert die Freundesliste");
		sendHelpMessage(sender, "/friend requests", "Zeigt alle Anfragen an");
		sendHelpMessage(sender, "/friend accept <Name>", "Nimmt eine Anfrage an");
		sendHelpMessage(sender, "/friend acceptAll", "Nimmt alle offenen Freundschaftsanfragen an");
		sendHelpMessage(sender, "/friend deny <Name>", "Lehnt eine Anfrage ab");
		sendHelpMessage(sender, "/friend denyAll", "Lehnt alle offenen Freundschaftsanfragen ab");
		sendHelpMessage(sender, "/friend jump <Name>", "Auf des Freundes Server springen");
		sendHelpMessage(sender, "/friend toggle", "Anfragen aktivieren & deaktivieren");
		sendHelpMessage(sender, "/friend togglemessage", "Online/Offline Nachrichten");
		sendHelpMessage(sender, "/friend togglejump", "Erlaube/Verbiete anderen Spielern zu dir zu springen");
		sendHelpMessage(sender, "/msg <Name> <Nachricht>", "Nachrichten versenden");
		sendHelpMessage(sender, "/r <Nachricht>", "Antwortet auf eine erhaltene Nachricht");
	}
	
	private void sendHelpMessage(CommandSender sender, String cmd, String description) {
		sender.sendMessage(" �a" + cmd + " �8" + description);
	}

}
