package me.nicedev.droopmc.friend.utils;

import java.util.List;

import me.nicedev.droopmc.friend.FriendSystem;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public abstract class BaseCommand extends Command {

	private String cmd;
	private List<Object> args;
	public FriendSystem p = FriendSystem.getPlugin();
	
	public BaseCommand(String cmd, List<Object> list) {
		super(cmd);
		this.cmd = cmd;
		this.args = list;
		
		p.getProxy().getPluginManager().registerCommand(p, this);
	}
	
	public String getCommand() {
		return this.cmd;
	}
	
	public void execute(CommandSender sender, String[] args) {
		
		if(args.length != 0) {
			for(Object argObject : this.args) {
				ArgumentCommand arg = (ArgumentCommand) argObject;
				if(args[0].equalsIgnoreCase(arg.getArgument())) {
					arg.executeCommand(sender, args);
					return;
				}
			}
		}

		executeCommand(sender, args);
		return;
	}
	
	public abstract void executeCommand(CommandSender sender, String[] args);	
}
