package me.nicedev.droopmc.friend.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import me.nicedev.droopmc.friend.FriendSystem;

public class FriendPlayer {

	private UUID uuid;
	
	private List<UUID> friends;
	private String friendString;
	
	private List<UUID> invites;
	private String inviteString;
	
	private boolean acceptInvites;
	
	public FriendPlayer(UUID uuid) {
		this.uuid = uuid;
		
		loadCache();
	}
	
	private void loadCache() {
		friends = new ArrayList<UUID>();
		
		try {
			ResultSet rs = FriendSystem.getPlugin().mysqlConn.prepareStatement("SELECT * FROM friends WHERE uuid='" 
					+ uuid.toString() + "'").executeQuery();
			
			if(!rs.next()) {
				FriendSystem.getPlugin().mysqlConn.prepareStatement("INSERT INTO friends (uuid, friends, invites, accept) VALUES ('" + 
						uuid.toString() + "', '', '', '1')").executeUpdate();
				
				rs = FriendSystem.getPlugin().mysqlConn.prepareStatement("SELECT * FROM friends WHERE uuid='" 
						+ uuid.toString() + "'").executeQuery();
				rs.next();
			}
			
			inviteString = rs.getString("invites");
			friendString = rs.getString("friends");
			acceptInvites = rs.getInt("accept") == 1;
			
			for(String invite : inviteString.split(";")) {
				if(invite.equals("")) continue;
				this.friends.add(UUID.fromString(invite));
			}
			
			for(String friend : friendString.split(";")) {
				if(friend.equals("")) continue;
				this.friends.add(UUID.fromString(friend));
			}
		} catch(Exception ex) { }
	}
	
	public List<UUID> getFriends() {
		return friends;
	}
	
	public String getFriendString() {
		return friendString;
	}
	
	public void addFriend(UUID uuid) {
		friendString += ";" + uuid.toString();
		friends.add(uuid);
		try {
			FriendSystem.getPlugin().mysqlConn.prepareStatement("UPDATE friends SET friends='" + friendString + "'"
					+ " WHERE uuid='" + uuid.toString() + "'");
		} catch(Exception ex) { }
		
		FriendSystem.getPlugin().friendPlayers.put(uuid, this);
	}
	
	public void removeFriend(UUID uuid) {
		friendString = friendString.replace(";" + uuid.toString(), "");
		friends.remove(uuid);
		
		try {
			FriendSystem.getPlugin().mysqlConn.prepareStatement("UPDATE friends SET friends='" + friendString + "'"
					+ " WHERE uuid='" + uuid.toString() + "'");
		} catch(Exception ex) { }
		
		FriendSystem.getPlugin().friendPlayers.put(uuid, this);
	}
	
	public List<UUID> getInvites() {
		return invites;
	}
	
	public String getInviteString() {
		return inviteString;
	}
	
	public void addInvite(UUID uuid) {
		inviteString += ";" + uuid.toString();
		invites.add(uuid);
		try {
			FriendSystem.getPlugin().mysqlConn.prepareStatement("UPDATE friends SET invites='" + inviteString + "'"
					+ " WHERE uuid='" + uuid.toString() + "'");
		} catch(Exception ex) { }
		
		FriendSystem.getPlugin().friendPlayers.put(uuid, this);
	}
	
	public void removeInvite(UUID uuid) {
		inviteString = friendString.replace(";" + uuid.toString(), "");
		invites.remove(uuid);
		
		try {
			FriendSystem.getPlugin().mysqlConn.prepareStatement("UPDATE friends SET invites='" + inviteString + "'"
					+ " WHERE uuid='" + uuid.toString() + "'");
		} catch(Exception ex) { }
		
		FriendSystem.getPlugin().friendPlayers.put(uuid, this);
	}
	
	public boolean isAcceptingInvites() {
		return acceptInvites;
	}
	
	public void setAcceptInvites(boolean accept) {
		this.acceptInvites = accept;
		
		try {
			FriendSystem.getPlugin().mysqlConn.prepareStatement("UPDATE friends SET accept='" + (accept ? "1" : "0") + "'"
					+ " WHERE uuid='" + uuid.toString() + "'");
		} catch(Exception ex) { }
		
		FriendSystem.getPlugin().friendPlayers.put(uuid, this);
	}
	
	public static boolean hasPlayedBefore(UUID uuid) {
		try {
			ResultSet rs = FriendSystem.getPlugin().mysqlConn.prepareStatement("SELECT * FROM friends WHERE uuid='" 
					+ uuid.toString() + "'").executeQuery();
			
			return rs.next();
		} catch (SQLException e) {
			return false;
		}
	}
	
	public static FriendPlayer getPlayer(UUID uuid) {
		return (FriendSystem.getPlugin().friendPlayers.containsKey(uuid) ? 
				FriendSystem.getPlugin().friendPlayers.get(uuid) : new FriendPlayer(uuid));
	}
	
}
