package me.nicedev.droopmc.friend.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class NameFetcher {
	
	public static String getName(UUID uuid) {
		String uuidString = uuid.toString().replace("-", "");
		try {
			JsonParser jsonParser = new JsonParser();
		    HttpURLConnection connection = (HttpURLConnection)new URL("https://api.mojang.com/user/profiles/" + uuidString + 
		    		"/names").openConnection();
		    JsonObject response = (JsonObject) jsonParser.parse(new InputStreamReader(connection.getInputStream()));
			 
		    return response.get("name").toString();
		} catch(Exception ex) {
			return "Unbekannt";
		}
	}

	  public static UUID getUUID(String username)
	  {
	    try
	    {
	      Gson gson = new Gson();
	      ProfileData data = new ProfileData(username);
	      String uuid = null;
	      for (int i = 1; i < 100.0D; i++)
	      {
	        PlayerProfile[] result = post(new URL("https://api.mojang.com/profiles/page/" + i), Proxy.NO_PROXY, gson.toJson(data).getBytes());
	        if (result.length == 0)
	        {
	          break;
	        }
	        uuid = result[0].getId();
	      }
	      return fromString(uuid);
	    }
	    catch (Exception localException) {
	    }
	    return null;
	  }

	  private static UUID fromString(String uuid)
	  {
	    return UUID.fromString(uuid.substring(0, 8) + "-" + uuid.substring(8, 12) + "-" + 
	      uuid.substring(12, 16) + "-" + uuid.substring(16, 20) + "-" + uuid.substring(20, 32));
	  }

	  private static PlayerProfile[] post(URL url, Proxy proxy, byte[] bytes) throws IOException
	  {
	    HttpURLConnection connection = (HttpURLConnection)url.openConnection(proxy);
	    connection.setRequestMethod("POST");
	    connection.setRequestProperty("Content-Type", "application/json");
	    connection.setDoInput(true);
	    connection.setDoOutput(true);

	    DataOutputStream out = new DataOutputStream(connection.getOutputStream());
	    out.write(bytes);
	    out.flush();
	    out.close();

	    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	    StringBuffer response = new StringBuffer();
	    String line;
	    while ((line = reader.readLine()) != null)
	    {
	      response.append(line);
	      response.append('\r');
	    }
	    reader.close();

	    return ((SearchResult)new Gson().fromJson(response.toString(), SearchResult.class)).getProfiles();
	  }

	  private static class PlayerProfile
	  {
	    private String id;

	    public String getId()
	    {
	      return this.id;
	    }
	  }

	  private static class ProfileData
	  {
	    private String name;
	    private String agent = "minecraft";

	    public ProfileData(String name)
	    {
	      this.name = name;
	    }
	  }

	  private static class SearchResult
	  {
	    private NameFetcher.PlayerProfile[] profiles;

	    public NameFetcher.PlayerProfile[] getProfiles()
	    {
	      return this.profiles;
	    }
	  }
}
