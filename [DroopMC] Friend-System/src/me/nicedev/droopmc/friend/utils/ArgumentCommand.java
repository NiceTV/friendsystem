package me.nicedev.droopmc.friend.utils;

import me.nicedev.droopmc.friend.FriendSystem;
import net.md_5.bungee.api.CommandSender;

import com.mysql.jdbc.Messages;

public abstract class ArgumentCommand {

	private String arg;
	public FriendSystem p = FriendSystem.getPlugin();
	
	public ArgumentCommand(String arg) {
		this.arg = arg;
	}

	public String getArgument() {
		return this.arg;
	}
	
	public abstract void execute(CommandSender sender, String[] args);
	
	public void executeCommand(CommandSender sender, String[] args) {
		String[] newArgs = new String[args.length - 1];
		int counter = 0;
		
		for(String arg : args) {
			if(arg.equals(args[0])) {
				continue;
			}
			
			newArgs[counter] = args[counter + 1];
			counter++;
		}
		
		execute(sender, newArgs);
	}
}
