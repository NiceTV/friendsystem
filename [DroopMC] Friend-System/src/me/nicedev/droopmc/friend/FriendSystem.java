package me.nicedev.droopmc.friend;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import me.nicedev.droopmc.friend.utils.FriendPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class FriendSystem extends Plugin {

	/* Variablen */
	public String prefix = "�8[�aFreunde�8] �r";
	public Connection mysqlConn;
	public Configuration configuration;
	
	/* Maps und Lists */
	public Map<UUID, FriendPlayer> friendPlayers = new HashMap<UUID, FriendPlayer>();
	
	@Override
	public void onEnable() {
		plugin = this;
		
		File configFile = new File(getDataFolder(), "config.yml");
		
		if(!configFile.getParentFile().exists()) {
			configFile.getParentFile().mkdirs();
		} 
		
		if(!configFile.exists()) {
			try {
				configFile.createNewFile();
			} catch(IOException e) { }
		}
		
		try {
			configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		addDefault(configuration, "sql.host", "localhost");
		addDefault(configuration, "sql.user", "root");
		addDefault(configuration, "sql.password", "password");
		addDefault(configuration, "sql.db", "db");
		
		try {
			mysqlConn = DriverManager.getConnection("jdbc:mysql://" + configuration.getString("sql.host") + "/" + 
					configuration.getString("sql.db"), configuration.getString("sql.user"), configuration.getString("sql.password"));
			mysqlConn.prepareStatement("CREATE TABLE IF NOT EXISTS friends "
					+ "(id int primary key NOT NULL AUTO_INCREMENT, uuid longtext, friends longtext, invites longtext, accept tinyint(1))")
					.executeUpdate();
		} catch(Exception ex) {
			System.err.println("[FriendSystem] Can't connect to the mysql-database!");
			return;
		}
	}
	
	@Override
	public void onDisable() {
		try {
			mysqlConn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/* AddDefault */
	public void addDefault(Configuration conf, String key, Object value) {
		if(conf.get(key) != null) return;
		
		conf.set(key, value);
	}
	
	/* Instance */
	public static FriendSystem getPlugin() {
		return plugin;
	}
	
	private static FriendSystem plugin;
	
}
